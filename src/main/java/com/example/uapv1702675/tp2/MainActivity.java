package com.example.uapv1702675.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity
{
    private BookDbHelper db ;
    private SimpleCursorAdapter adapter;
    private long selectedItemID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new BookDbHelper(getApplicationContext());
        if ( db.bookNumber() == 0 )
            db.populate();
        final Cursor cursor = db.fetchAllBooks();
        adapter = new SimpleCursorAdapter(MainActivity.this, android.R.layout.simple_list_item_2, cursor, new String[]  {BookDbHelper.COLUMN_BOOK_TITLE,BookDbHelper.COLUMN_AUTHORS}, new int[] {android.R.id.text1, android.R.id.text2},0);
        final ListView list = (ListView) findViewById(R.id.bookList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

        final Intent intent = new Intent(this,BookActivity.class);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Cursor cursor = (Cursor)list.getItemAtPosition(position);
                Book book = db.cursorToBook(cursor);
                intent.putExtra("book",book);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Book book = new Book("","","","","");
                intent.putExtra("book",book);
                Intent addBook = new Intent(getApplicationContext(),BookActivity.class);
                startActivity(addBook);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        ListView l = (ListView) v;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Cursor item = (Cursor) l.getItemAtPosition(info.position);
        selectedItemID = item.getLong(item.getColumnIndexOrThrow(BookDbHelper._ID));
        menu.setHeaderTitle(item.getString(item.getColumnIndexOrThrow(BookDbHelper.COLUMN_BOOK_TITLE)));
        menu.add(Menu.NONE, R.id.delete, Menu.NONE, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.delete:
                db.deleteBook(selectedItemID);
                Cursor c = db.fetchAllBooks();
                adapter.changeCursor(c);
                adapter.notifyDataSetChanged();
                return true;

        }
        return super.onContextItemSelected(item);
    }
}
