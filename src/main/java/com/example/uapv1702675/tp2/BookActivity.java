package com.example.uapv1702675.tp2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity
{
    Book book;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final BookDbHelper db = new BookDbHelper(getApplicationContext());
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book);

        final EditText eEditBookTitle = (EditText)findViewById(R.id.nameBook);
        final EditText eEditAuthors = (EditText)findViewById(R.id.editAuthors);
        final EditText eEditYear = (EditText)findViewById(R.id.editYear);
        final EditText eEditGenres = (EditText)findViewById(R.id.editGenres);
        final EditText eEditPublisher = (EditText)findViewById(R.id.editPublisher);
        final Button eBtn1 = (Button)findViewById(R.id.button);

        Intent intent = getIntent();
        Bundle bn = intent.getBundleExtra("book");

        book = (Book) intent.getParcelableExtra("book");
        final Intent intent1 = new Intent(this, MainActivity.class);

        if ( book != null )
        {
            eEditBookTitle.setText(book.getTitle());
            eEditAuthors.setText(book.getAuthors());
            eEditYear.setText(book.getYear());
            eEditGenres.setText(book.getGenres());
            eEditPublisher.setText(book.getPublisher());
            eBtn1.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    if ( String.valueOf(eEditBookTitle.getText()).equals("") == true )
                    {
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("Title ERROR");
                        alertDialog.setMessage(" Title field shouldn't be empty");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL",
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    else {
                        book.setTitle(String.valueOf(eEditBookTitle.getText()));
                        book.setAuthors(String.valueOf(eEditAuthors.getText()));
                        book.setYear(String.valueOf(eEditYear.getText()));
                        book.setGenres(String.valueOf(eEditGenres.getText()));
                        book.setPublisher(String.valueOf(eEditPublisher.getText()));
                        db.updateBook(book);
                        final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(BookActivity.this, "Save ...", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        else
        {
            eBtn1.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    if ( String.valueOf(eEditBookTitle.getText()).equals("") == true )
                    {
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("Title ERROR");
                        alertDialog.setMessage(" Title field shouldn't be empty");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CANCEL",
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    else {
                        book.setTitle(String.valueOf(eEditBookTitle.getText()));
                        book.setAuthors(String.valueOf(eEditAuthors.getText()));
                        book.setYear(String.valueOf(eEditYear.getText()));
                        book.setGenres(String.valueOf(eEditGenres.getText()));
                        book.setPublisher(String.valueOf(eEditPublisher.getText()));
                        db.addBook(book);
                        final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(BookActivity.this, "Save ...", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }
}
