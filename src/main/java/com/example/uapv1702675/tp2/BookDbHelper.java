package com.example.uapv1702675.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_BOOK_TITLE + " TEXT, " + COLUMN_AUTHORS + " TEXT, " + COLUMN_YEAR + " DATE, " + COLUMN_GENRES + " TEXT, " + COLUMN_PUBLISHER + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // toDoLOL
    }


    public boolean addBook(Book book)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues bookContent=new ContentValues();
        bookContent.put(COLUMN_BOOK_TITLE, book.getTitle());
        bookContent.put(COLUMN_AUTHORS, book.getAuthors());
        bookContent.put(COLUMN_YEAR, book.getYear());
        bookContent.put(COLUMN_GENRES, book.getGenres());
        bookContent.put(COLUMN_PUBLISHER, book.getPublisher());
        if ( db.insert(TABLE_NAME,null,bookContent) != -1)
        {
            db.close();
            return true;
        }
        return false;

    }


    public int updateBook(Book book)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;
        ContentValues bookContent=new ContentValues();
        bookContent.put(COLUMN_BOOK_TITLE, book.getTitle());
        bookContent.put(COLUMN_AUTHORS, book.getAuthors());
        bookContent.put(COLUMN_YEAR, book.getYear());
        bookContent.put(COLUMN_GENRES, book.getGenres());
        bookContent.put(COLUMN_PUBLISHER, book.getPublisher());
        res = db.update(TABLE_NAME, bookContent, "_id = ?",new String[] { String.valueOf(book.getId()) });
        return res;
    }

    public Cursor fetchAllBooks()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {_ID,COLUMN_BOOK_TITLE,COLUMN_AUTHORS,COLUMN_YEAR,COLUMN_GENRES,COLUMN_PUBLISHER};
        Cursor cursor = db.query(TABLE_NAME,columns,null,null,null,null,null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor;
    }

    public void deleteBook(long bookID)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID+"="+bookID, null);
        db.close();
    }

    public void populate()
    {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }

    public long bookNumber()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
    }

    public static Book cursorToBook(Cursor cursor)
    {
        Book book = null;
        // build a Book object from cursor
        int id_index = cursor.getColumnIndexOrThrow(_ID);
        int title_index = cursor.getColumnIndexOrThrow(COLUMN_BOOK_TITLE);
        int authors_index = cursor.getColumnIndexOrThrow(COLUMN_AUTHORS);
        int year_index = cursor.getColumnIndexOrThrow(COLUMN_YEAR);
        int genres_index = cursor.getColumnIndexOrThrow(COLUMN_GENRES);
        int publisher_index = cursor.getColumnIndexOrThrow(COLUMN_PUBLISHER);

        long id = (long) cursor.getLong(id_index);
        String title = (String) cursor.getString(title_index);
        String authors = (String) cursor.getString(authors_index);
        String year = (String) cursor.getString(year_index);
        String genres = (String) cursor.getString(genres_index);
        String publisher = (String) cursor.getString(publisher_index);

        return new Book(id, title, authors, year, genres, publisher);
    }
}
